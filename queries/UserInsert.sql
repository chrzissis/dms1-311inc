--DROP TABLE public.roles;

CREATE TABLE public.roles
(
  id integer PRIMARY KEY,
  name character varying(50)
);

--DROP TABLE public.users;

CREATE TABLE public.users
(
  userId integer PRIMARY KEY,
  FirstName character varying(50),
  LastName character varying(50),
  LoginName character varying(50),
  eMail character varying(50),
  password character varying(1000),
  address character varying(50));


--DROP TABLE public.user_roles;

CREATE TABLE public.user_roles
(
  id integer NOT NULL,
  user_id integer PRIMARY KEY,
  role_id integer,
  CONSTRAINT roles_id_fkey FOREIGN KEY (role_id)
      REFERENCES public.roles (id),
  CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public.users (userId) )