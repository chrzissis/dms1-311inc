package di.chicago.web.repository;

import di.chicago.web.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}