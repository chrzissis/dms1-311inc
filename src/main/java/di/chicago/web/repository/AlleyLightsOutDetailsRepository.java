package di.chicago.web.repository;

import di.chicago.web.model.entities.AlleyLightsOutDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlleyLightsOutDetailsRepository extends JpaRepository<AlleyLightsOutDetails, Integer> {
}
