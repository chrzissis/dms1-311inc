package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sanitation_code_complaints_details", schema = "public", catalog = "DI_Chicago_")
public class SanitationCodeComplaintsDetails extends SuperDetails{
    private String natureofcodeviolation;

    public SanitationCodeComplaintsDetails() {
    }

    public SanitationCodeComplaintsDetails(PostRequestParameter other) {
        this.natureofcodeviolation = other.natureofcodeviolation;
    }

    public SanitationCodeComplaintsDetails(SanitationCodeComplaintsDetails other) {
        this.natureofcodeviolation = other.natureofcodeviolation;
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "natureofcodeviolation")
    public String getNatureofcodeviolation() {
        return natureofcodeviolation;
    }

    public void setNatureofcodeviolation(String natureofcodeviolation) {
        this.natureofcodeviolation = natureofcodeviolation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SanitationCodeComplaintsDetails that = (SanitationCodeComplaintsDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(natureofcodeviolation, that.natureofcodeviolation);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, natureofcodeviolation);
    }


}
