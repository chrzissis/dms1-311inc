package di.chicago.web.repository;

import di.chicago.web.model.entities.VacantAndAbandonedBuildingReportedDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VacantAndAbandonedBuildingReportedDetailsRepository extends JpaRepository<VacantAndAbandonedBuildingReportedDetails, Integer> {
}
