package di.chicago.web.repository;

import di.chicago.web.model.entities.StreetLightsAllOutDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StreetLightsAllOutDetailsRepository extends JpaRepository<StreetLightsAllOutDetails, Integer> {
}
