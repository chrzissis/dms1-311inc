/* importing data - panos */
COPY Abandoned_Vehicles
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-abandoned-vehicles.csv' DELIMITERS ',' CSV;

COPY Alley_Lights_Out
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-alley-lights-out.csv' DELIMITERS ',' CSV;

COPY Garbage_Carts
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-garbage-carts.csv' DELIMITERS ',' CSV;

COPY Graffiti_Removal
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-graffiti-removal.csv' DELIMITERS ',' CSV;

COPY Pot_Holes_Reported
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-pot-holes-reported.csv' DELIMITERS ',' CSV;

COPY Rodent_Baiting
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-rodent-baiting.csv' DELIMITERS ',' CSV;

COPY Sanitation_Code_Complaints
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-sanitation-code-complaints.csv' DELIMITERS ',' CSV;

COPY Street_Lights_All_Out
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-street-lights-all-out.csv' DELIMITERS ',' CSV;

COPY Street_Lights_One_Out
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-street-lights-one-out.csv' DELIMITERS ',' CSV;

COPY Tree_Debris
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-tree-debris.csv' DELIMITERS ',' CSV;

COPY Tree_Trims
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-tree-trims.csv' DELIMITERS ',' CSV;

COPY Vacant_And_Abandoned_Building_Reported
FROM '/home/panagiotis/Downloads/chicago-311-service-requests/311-service-requests-vacant-and-abandoned-buildings-reported.csv' DELIMITERS ',' CSV;
