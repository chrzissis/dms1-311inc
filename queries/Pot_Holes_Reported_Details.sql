drop table Pot_Holes_Reported_Details;
create table Pot_Holes_Reported_Details (
  RequestDetailsId serial primary key,RequestId int,
  CurrentActivity varchar(1000) null,
  MostRecentAction varchar(1000) null,
  NumberOfPotholesFilledOnBlock varchar(1000) null,
  SSA varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);

insert into Pot_Holes_Reported_Details (RequestId,CurrentActivity,MostRecentAction,NumberofPotholesFilledOnBlock,SSA)
select r.requestid,CurrentActivity,MostRecentAction,NumberofPotholesFilledOnBlock,SSA
from Pot_Holes_Reported phr
inner join request r on r.servicerequestnumber = phr.servicerequestnumber
