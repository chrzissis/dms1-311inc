package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "alley_lights_out_details", schema = "public", catalog = "DI_Chicago_")
public class AlleyLightsOutDetails extends SuperDetails{

    public AlleyLightsOutDetails() {
    }

    public AlleyLightsOutDetails(PostRequestParameter other) {
    }

    public AlleyLightsOutDetails(AlleyLightsOutDetails other) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlleyLightsOutDetails that = (AlleyLightsOutDetails) o;
        return requestdetailsid == that.requestdetailsid;
    }
    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid);
    }

}
