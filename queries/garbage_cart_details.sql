drop table Garbage_Carts_Details
create table Garbage_Carts_Details (
  RequestDetailsId serial primary key,RequestId int,
  NumberOfBlackCartsDelivered varchar(1000) null,
  CurrentActivity varchar(1000) null,
  MostRecentAction varchar(1000) null,
  SSA varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);


insert into Garbage_Carts_Details (requestId, NumberOfBlackCartsDelivered, CurrentActivity, MostRecentAction, SSA)
select r.requestId, gc.NumberOfBlackCartsDelivered, gc.CurrentActivity, gc.MostRecentAction, gc.SSA
from Garbage_Carts gc
inner join request r on r.servicerequestnumber = gc.servicerequestnumber
