drop table Alley_Lights_Out_Details;
/* no need */
create table Alley_Lights_Out_Details (
  RequestDetailsId serial primary key,RequestId int,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);

drop table Street_Lights_All_Out_Details;
/* no need */
create table Street_Lights_All_Out_Details (
  RequestDetailsId serial primary key,RequestId int,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);

drop table Street_Lights_One_Out_Details;
/* no need */
create table Street_Lights_One_Out_Details (
 RequestDetailsId serial primary key,RequestId int,
 FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);


drop table Request;
drop table Request_Statuses;
drop table Request_Types;

create table Request_Statuses (
  StatusId serial primary key,
  Code varchar(100) not null,
  Name varchar(100) not null
);


create table Request_Types (
  TypeId serial primary key,
  Code varchar(100) not null,
  Name varchar(100) not null
);


create table Request (
  RequestId serial unique Primary key identity ,
  ServiceRequestNumber varchar (20),
  CreatedOn date,
  UpdatedOn date,
  StatusId int not null,
  CompletedOn date null,
  TypeId int not null,
  StreetAddress varchar(1000) null,
  ZIPCode varchar(1000) null,
  XCoordinate varchar(1000) null,
  YCoordinate varchar(1000) null,
  Ward varchar(1000) null,
  PoliceDistrict varchar(1000) null,
  CommunityArea varchar(1000) null,
  Latitude double precision null ,
  Longitude double precision null,
  Location varchar(1000) null,
  FOREIGN key (StatusId) references Request_Statuses (StatusId),
  FOREIGN key (TypeId) references Request_Types (TypeId),
);



with types as (
select distinct Status from Abandoned_Vehicles
union select distinct Status from Alley_Lights_Out
union select distinct Status from Garbage_Carts
union select distinct Status from Graffiti_Removal
union select distinct Status from Pot_Holes_Reported
union select distinct Status from Rodent_Baiting
union select distinct Status from Sanitation_Code_Complaints
union select distinct Status from Street_Lights_All_Out
union select distinct Status from Street_Lights_One_Out
union select distinct Status from Tree_Debris
union select distinct Status from Tree_Trims
)
insert into Request_Statuses(Name,Code)

select Status, case
                 when Status = 'Open' then 'open'
                 when Status = 'Open - Dup' then 'open-dup'
                 when Status = 'Completed' then 'closed'
                 when Status = 'Completed - Dup' then 'closed-dup'
  end
from types;


     with types as (
     select distinct TypeofServiceRequest from Abandoned_Vehicles
     union select distinct TypeofServiceRequest from Alley_Lights_Out
     union select distinct TypeofServiceRequest from Garbage_Carts
     union select distinct TypeofServiceRequest from Graffiti_Removal
     union select distinct TypeofServiceRequest from Pot_Holes_Reported
     union select distinct TypeofServiceRequest from Rodent_Baiting
     union select distinct TypeofServiceRequest from Sanitation_Code_Complaints
     union select distinct TypeofServiceRequest from Street_Lights_All_Out
     union select distinct TypeofServiceRequest from Street_Lights_One_Out
     union select distinct TypeofServiceRequest from Tree_Debris
     union select distinct TypeofServiceRequest from Tree_Trims
     union select distinct ServiceRequestType from Vacant_And_Abandoned_Building_Reported
  )
insert into Request_Types(Name,Code)

select distinct case
                  when TypeofServiceRequest = 'Pot Hole in Street' then 'Pothole in Street'
                  when TypeofServiceRequest = 'Street Light - 1/Out' then 'Street Light Out'
                  else TypeofServiceRequest end,
                case
                                                   when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
                                                   when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
                                                   when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
                                                   when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
                                                   when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
                                                   when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
                                                   when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
                                                   when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
                                                   when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
                                                   when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
                                                   when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
                                                   when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'
                                                   when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

                  end
from types;



select * from Request_Statuses;
select * from request_types;
