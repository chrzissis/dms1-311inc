/* query one - takes start and end date - no hour specified */
CREATE or REPLACE FUNCTION sys_GetCounterOfAllRequestTypes(date, date) returns table(name varchar(100) , counter bigint)
AS 'select rt.NAME, count(*) counter_
  from ( select * from request
   where CreatedOn > $1 and CreatedOn < $2
  ) r
  inner join request_types rt on rt.TypeId = r.TypeId
  group by rt.name
  order by counter_ desc;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetCounterOfAllRequestTypes(date '2016-01-01', date '2016-03-01')


/* query two - takes start, end date and request type code - no hour specified */
CREATE or REPLACE FUNCTION sys_GetCounterOfSpecificRequestType(date, date, varchar(100)) returns table(name varchar(100) ,createdOn date , counter bigint)
AS 'select rt.Name, r.CreatedOn, count(*) counter_
  from ( select * from request
   where CreatedOn > $1 and CreatedOn < $2
  ) r
  inner join request_types rt on rt.TypeId = r.TypeId and rt.code = $3
  group by rt.name, r.CreatedOn
  order by r.createdOn desc;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetCounterOfSpecificRequestType(date '2016-01-01', date '2016-03-01', 'rodent-rat')


/* query three - takes request type code and date specified */
CREATE or REPLACE FUNCTION sys_GetMostCommonRequestTypeOnDayPerZipCode(varchar(100), date) returns table(zipcode varchar(100) ,name varchar(100))
AS 'with stats as (
    select r.zipcode,rt.Name, count(*) counter_
    from ( select * from request
     where date_trunc(''day'', CreatedOn) = $2 ) r
    inner join request_types rt on rt.typeid = r.typeid
    group by r.zipcode, rt.Name
    order by counter_ desc
  )
  select distinct r.zipcode, coalesce( ( select stats.name from stats where stats.zipcode = r.zipcode order by stats.counter_ limit 1 ), '''')
  from stats r
  where r.zipcode = $1'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetMostCommonRequestTypeOnDayPerZipCode('60632',date '2015-01-01')

/*query four - average completion date between two dates*/

CREATE or REPLACE FUNCTION sys_GetAvgCompetionPerRequest(date, date) returns numeric
AS 'select avg(req.completedon - req.createdon)
    from request req
    where req.createdon >= $1 and req.completedon<=$2 and req.completedon is not null;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetAvgCompetionPerRequest(date '2015-01-01',date '2015-01-05')


/*query five - most common in the box in a day*/
CREATE or REPLACE FUNCTION sys_GetMostCommonTypeInBox(double precision , double precision , double precision , double precision, date) returns table(name varchar(100))
AS '
select request_types.name--, counter
from(
 SELECT req.typeid as typeid, count(req.requestid) as counter
    from request req
    where req.latitude <= $3
    and req.latitude>=$1
    and req.longitude <=$4
    and req.longitude >= $2
    and date_trunc(''day'',req.createdon) = date_trunc(''day'',$5)
    group by req.typeid
    order by count(requestid) desc
    limit 1
) as recount, request_types
where recount.typeid = request_types.typeid;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetMostCommonTypeInBox(0,-180,90,180,date '2010-05-01')

/* query six - top ssa areas per request type - takes date from and to */
CREATE or REPLACE FUNCTION sys_GetTopSSAPerRequestType(date, date) returns table(name varchar(100) , SSA varchar(1000))
AS 'with results as (select rt.Name,
          case
            when avd.SSA is not null then avd.SSA
            when gcd.SSA is not null then gcd.SSA
            when grd.SSA is not null then grd.SSA
            when phrd.SSA is not null then phrd.SSA
            end                                                           as SSA_,

          count(*)                                                           counter_,
          ROW_NUMBER() over (partition by rt.Name order by count(*) desc) as RowId
   from ( select * from request
   where CreatedOn > $1 and CreatedOn < $2
  ) r
          left join Abandoned_Vehicles_Details avd on r.typeid = 1 and avd.RequestId = r.RequestId and avd.SSA is not null
          left join Garbage_Carts_Details gcd on r.typeid = 3 and gcd.RequestId = r.RequestId and gcd.SSA is not null
          left join Graffiti_Removal_Details grd on r.typeid = 4 and grd.RequestId = r.RequestId and grd.SSA is not null
          left join Pot_Holes_Reported_Details phrd on r.typeid = 5 and phrd.RequestId = r.RequestId and phrd.SSA is not null
          inner join request_types rt on rt.TypeId = r.TypeId
   where (case
          when avd.SSA is not null then avd.SSA
          when gcd.SSA is not null then gcd.SSA
          when grd.SSA is not null then grd.SSA
          when phrd.SSA is not null then phrd.SSA
     end) is not null
   group by rt.name, SSA_
   order by counter_ desc
)
select Name, SSA_ as SSA
from results
where RowId = 1
;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetTopSSAPerRequestType(date '2010-01-01',date '2018-01-01')

/*query seven - Find the license plates (if any) that have been involved in abandoned vehicle complaints more than once*/
CREATE or REPLACE FUNCTION sys_GetFrequentlyAbandonedVehiclesPlates() returns table(licenseplate varchar(100))
AS
'select licenseplate
from abandoned_vehicles_details
group by licenseplate
having count(distinct(abandoned_vehicles_details.requestid)) > 1 and length(licenseplate) > 0;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetFrequentlyAbandonedVehiclesPlates()

/*query eight - Find the second most common color of vehicles involved in abandoned vehicle complaints*/
CREATE or REPLACE FUNCTION sys_GetMostHatedVehicleColor() returns varchar(100)
AS
'select vehiclecolor
from (
      select vehiclecolor , count(distinct(abandoned_vehicles_details.requestid)) as counter
      from abandoned_vehicles_details
      group by vehiclecolor
      order by count(distinct(abandoned_vehicles_details.requestid)) desc
      limit 2
      ) toptwo
      order by counter asc
limit 1;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetMostHatedVehicleColor()


/*query nine - Find the rodent baiting requests where the number of premises baited is less than a specified number.*/
CREATE or REPLACE FUNCTION sys_GetBaitedPremisesLessThan(int) returns table(servicerequestnumber varchar(100))
AS
'SELECT r.servicerequestnumber
from rodent_baiting_details rbd
 inner join request r on r.requestid = rbd.requestid
where numberofpremisesbaited < $1;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetBaitedPremisesLessThan(1)


/*query ten*/
CREATE or REPLACE FUNCTION sys_GetGarbagePremisesLessThan(int) returns table(servicerequestnumber varchar(100))
AS
'SELECT r.servicerequestnumber
from rodent_baiting_details rbd
 inner join request r on r.requestid = rbd.requestid
where NumberOfPremisesWithGarbage < $1;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetGarbagePremisesLessThan(1)

/*query eleven*/
CREATE or REPLACE FUNCTION sys_GetRatPremisesLessThan(int) returns table(servicerequestnumber varchar(100))
AS
'SELECT r.servicerequestnumber
from rodent_baiting_details rbd
 inner join request r on r.requestid = rbd.requestid
where NumberOfPremisesWithRats < $1;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetRatPremisesLessThan(1)


/* query twelve - asks for a specific day as date input and gets truncated*/
CREATE or REPLACE FUNCTION sys_GetBusyPoliceDistricts(date ) returns table(policeDistrictId varchar(100))
AS
'with potholesHandled as (
select distinct policedistrict, count(*) count_
from request
where typeid = 5 and date_trunc(''day'',createdon) = date_trunc(''day'',$1)
group by policedistrict
having count(*) > 1),
rodentHandled as (
select distinct policedistrict, count(*) count_
from request
where typeid = 6 and date_trunc(''day'',createdon) = date_trunc(''day'', $1)
group by policedistrict
having count(*) > 1)
select p.policedistrict
from potholesHandled p
inner join rodentHandled r on r.policedistrict = p.policedistrict;'
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;
--select * from sys_GetBusyPoliceDistricts(date '2013-05-05')
