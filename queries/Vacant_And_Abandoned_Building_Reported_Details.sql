drop table Vacant_And_Abandoned_Building_Reported_Details;
create table Vacant_And_Abandoned_Building_Reported_Details (
  RequestDetailsId serial primary key,RequestId int,
  ServiceRequestType varchar(1000) null,
  BuildingLocation_LOT_BGD varchar(1000) null,
  DangerousOrHazardous varchar(1000) null,
  OpenOrBoarded varchar(1000) null,
  IfOpenEntryPoint varchar(1000) null,
  VacantOrOccupied varchar(1000) null,
  VacantDueToFire varchar(1000) null,
  PeopleUsingProperty varchar(1000) null,
  AddressStreetNumber varchar(1000) null,
  AddressStreetDirection varchar(1000) null,
  AddressStreetName varchar(1000) null,
  AddressStreetSuffix varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Vacant_And_Abandoned_Building_Reported_Details(RequestId,ServiceRequestType,BuildingLocation_LOT_BGD,DangerousOrHazardous,OpenOrBoarded,IfOpenEntryPoint,VacantOrOccupied,VacantDueToFire,PeopleUsingProperty,  AddressStreetNumber,AddressStreetDirection ,AddressStreetName,AddressStreetSuffix )
select r.RequestId,ServiceRequestType,BuildingLocation_LOT_BGD,DangerousOrHazardous,OpenOrBoarded,IfOpenEntryPoint,VacantOrOccupied,VacantDueToFire,PeopleUsingProperty,  AddressStreetNumber,AddressStreetDirection ,AddressStreetName,AddressStreetSuffix
from vacant_and_abandoned_building_reported vabr
inner join request r on r.servicerequestnumber = vabr.servicerequestnumber