package di.chicago.web.web;


import di.chicago.web.model.User;
import di.chicago.web.model.entities.Request;
import di.chicago.web.service.RequestService;
import di.chicago.web.service.SecurityService;
import di.chicago.web.service.UserService;
import di.chicago.web.validator.UserValidator;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;


@Controller
@RequestMapping("/")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private RequestService requestService;

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "/registration.html";
    }

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        if(userForm.getFirstName()==null || userForm.getFirstName()==""){
            userForm.setFirstName("Visitor");
        }

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "/registration.html";
        }

        userService.save(userForm);
        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "/login.html";
    }

    @RequestMapping(value = {"", "welcome"}, method = RequestMethod.GET)
    public String welcome(Model model, Principal principal) {
        model.addAttribute("user",getUser(principal));
        return "/welcome.html";
    }

    @RequestMapping(value =  "home", method = RequestMethod.GET)
    public String home(Model model) {
        return "/home.html";
    }

    //This is how you get the user details
    public User getUser(Principal principal) {
        return userService.findByUsername(principal.getName());
    }

}