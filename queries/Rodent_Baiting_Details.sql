drop table Rodent_Baiting_Details;
create table Rodent_Baiting_Details (
  RequestDetailsId serial primary key,RequestId int,
  NumberOfPremisesBaited int null,
  NumberOfPremisesWithGarbage int null,
  NumberOfPremisesWithRats int null,
  CurrentActivity varchar(1000) null,
  MostRecentAction varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Rodent_Baiting_Details (RequestId,NumberOfPremisesBaited,NumberOfPremisesWithGarbage,NumberOfPremisesWithRats,CurrentActivity,MostRecentAction)
select r.RequestId,cast(NumberOfPremisesBaited as double precision),cast(NumberOfPremisesWithGarbage as double precision),cast(NumberOfPremisesWithRats as double precision),CurrentActivity,MostRecentAction
from Rodent_Baiting rb
inner join request r on r.servicerequestnumber = rb.servicerequestnumber