package di.chicago.web.service;

import di.chicago.web.model.entities.Request;
import di.chicago.web.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;


@Service
public class RequestServiceImpl implements RequestService {
    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Request request) {
        requestRepository.save(request);
    }

    @Override
    @Transactional
    public void removeById(Long id) {
        requestRepository.removeByRequestid(id);
    }


    @Override
    public Request findByRequestID(int id) {

        return requestRepository.getOne(id);
    }



    @Override
    public TypedQuery searchRequest(Request search) {            ///**Get HTMLspecialchars checking here
        String qstring = "select request from Request request where 1=1";
        if(search.getServicerequestnumber()!=null && search.getServicerequestnumber()!=""){
            qstring=qstring+" and request.servicerequestnumber = '"+search.getServicerequestnumber()+"'";
        }
        if(search.getStreetaddress()!=null && search.getStreetaddress()!=""){
            qstring=qstring+" and request.streetaddress like '%"+search.getStreetaddress()+"%'";
        }
        if(search.getZipcode()!=null && search.getZipcode()!=""){
            qstring=qstring+" and request.zipcode = '"+search.getZipcode()+"'";
        }
        qstring+=" order by CreatedOn desc ";
        TypedQuery query = entityManager.createQuery( qstring, Request.class);
        return query;
    }


    public List<Request> getQueryPage(int pageNo, TypedQuery query){
        Integer min = (pageNo - 1) * 20;
        if(min<0) min=0;
        List<Request> result = query.setMaxResults(20).setFirstResult(min).getResultList();

        return result;//.subList(min,max);
    }

}