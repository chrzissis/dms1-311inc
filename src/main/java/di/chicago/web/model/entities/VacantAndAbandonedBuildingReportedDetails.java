package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "vacant_and_abandoned_building_reported_details", schema = "public", catalog = "DI_Chicago_")
public class VacantAndAbandonedBuildingReportedDetails extends SuperDetails{
    private String servicerequesttype;
    private String buildinglocationLotBgd;
    private String dangerousorhazardous;
    private String openorboarded;
    private String ifopenentrypoint;
    private String vacantoroccupied;
    private String vacantduetofire;
    private String peopleusingproperty;
    private String addressstreetnumber;
    private String addressstreetdirection;
    private String addressstreetname;
    private String addressstreetsuffix;

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    public VacantAndAbandonedBuildingReportedDetails(VacantAndAbandonedBuildingReportedDetails other) {
        this.servicerequesttype = other.servicerequesttype;
        this.buildinglocationLotBgd = other.buildinglocationLotBgd;
        this.dangerousorhazardous = other.dangerousorhazardous;
        this.openorboarded = other.openorboarded;
        this.ifopenentrypoint = other.ifopenentrypoint;
        this.vacantoroccupied = other.vacantoroccupied;
        this.vacantduetofire = other.vacantduetofire;
        this.peopleusingproperty = other.peopleusingproperty;
        this.addressstreetnumber = other.addressstreetnumber;
        this.addressstreetdirection = other.addressstreetdirection;
        this.addressstreetname = other.addressstreetname;
        this.addressstreetsuffix = other.addressstreetsuffix;
    }

    public VacantAndAbandonedBuildingReportedDetails(PostRequestParameter other) {
        this.servicerequesttype = other.servicerequesttype;
        this.buildinglocationLotBgd = other.buildinglocationLotBgd;
        this.dangerousorhazardous = other.dangerousorhazardous;
        this.openorboarded = other.openorboarded;
        this.ifopenentrypoint = other.ifopenentrypoint;
        this.vacantoroccupied = other.vacantoroccupied;
        this.vacantduetofire = other.vacantduetofire;
        this.peopleusingproperty = other.peopleusingproperty;
        this.addressstreetnumber = other.addressstreetnumber;
        this.addressstreetdirection = other.addressstreetdirection;
        this.addressstreetname = other.addressstreetname;
        this.addressstreetsuffix = other.addressstreetsuffix;
    }

    public VacantAndAbandonedBuildingReportedDetails() {
    }

    @Basic
    @Column(name = "servicerequesttype")
    public String getServicerequesttype() {
        return servicerequesttype;
    }

    public void setServicerequesttype(String servicerequesttype) {
        this.servicerequesttype = servicerequesttype;
    }

    @Basic
    @Column(name = "buildinglocation_lot_bgd")
    public String getBuildinglocationLotBgd() {
        return buildinglocationLotBgd;
    }

    public void setBuildinglocationLotBgd(String buildinglocationLotBgd) {
        this.buildinglocationLotBgd = buildinglocationLotBgd;
    }

    @Basic
    @Column(name = "dangerousorhazardous")
    public String getDangerousorhazardous() {
        return dangerousorhazardous;
    }

    public void setDangerousorhazardous(String dangerousorhazardous) {
        this.dangerousorhazardous = dangerousorhazardous;
    }

    @Basic
    @Column(name = "openorboarded")
    public String getOpenorboarded() {
        return openorboarded;
    }

    public void setOpenorboarded(String openorboarded) {
        this.openorboarded = openorboarded;
    }

    @Basic
    @Column(name = "ifopenentrypoint")
    public String getIfopenentrypoint() {
        return ifopenentrypoint;
    }

    public void setIfopenentrypoint(String ifopenentrypoint) {
        this.ifopenentrypoint = ifopenentrypoint;
    }

    @Basic
    @Column(name = "vacantoroccupied")
    public String getVacantoroccupied() {
        return vacantoroccupied;
    }

    public void setVacantoroccupied(String vacantoroccupied) {
        this.vacantoroccupied = vacantoroccupied;
    }

    @Basic
    @Column(name = "vacantduetofire")
    public String getVacantduetofire() {
        return vacantduetofire;
    }

    public void setVacantduetofire(String vacantduetofire) {
        this.vacantduetofire = vacantduetofire;
    }

    @Basic
    @Column(name = "peopleusingproperty")
    public String getPeopleusingproperty() {
        return peopleusingproperty;
    }

    public void setPeopleusingproperty(String peopleusingproperty) {
        this.peopleusingproperty = peopleusingproperty;
    }

    @Basic
    @Column(name = "addressstreetnumber")
    public String getAddressstreetnumber() {
        return addressstreetnumber;
    }

    public void setAddressstreetnumber(String addressstreetnumber) {
        this.addressstreetnumber = addressstreetnumber;
    }

    @Basic
    @Column(name = "addressstreetdirection")
    public String getAddressstreetdirection() {
        return addressstreetdirection;
    }

    public void setAddressstreetdirection(String addressstreetdirection) {
        this.addressstreetdirection = addressstreetdirection;
    }

    @Basic
    @Column(name = "addressstreetname")
    public String getAddressstreetname() {
        return addressstreetname;
    }

    public void setAddressstreetname(String addressstreetname) {
        this.addressstreetname = addressstreetname;
    }

    @Basic
    @Column(name = "addressstreetsuffix")
    public String getAddressstreetsuffix() {
        return addressstreetsuffix;
    }

    public void setAddressstreetsuffix(String addressstreetsuffix) {
        this.addressstreetsuffix = addressstreetsuffix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VacantAndAbandonedBuildingReportedDetails that = (VacantAndAbandonedBuildingReportedDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(servicerequesttype, that.servicerequesttype) &&
                Objects.equals(buildinglocationLotBgd, that.buildinglocationLotBgd) &&
                Objects.equals(dangerousorhazardous, that.dangerousorhazardous) &&
                Objects.equals(openorboarded, that.openorboarded) &&
                Objects.equals(ifopenentrypoint, that.ifopenentrypoint) &&
                Objects.equals(vacantoroccupied, that.vacantoroccupied) &&
                Objects.equals(vacantduetofire, that.vacantduetofire) &&
                Objects.equals(peopleusingproperty, that.peopleusingproperty) &&
                Objects.equals(addressstreetnumber, that.addressstreetnumber) &&
                Objects.equals(addressstreetdirection, that.addressstreetdirection) &&
                Objects.equals(addressstreetname, that.addressstreetname) &&
                Objects.equals(addressstreetsuffix, that.addressstreetsuffix);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, servicerequesttype, buildinglocationLotBgd, dangerousorhazardous, openorboarded, ifopenentrypoint, vacantoroccupied, vacantduetofire, peopleusingproperty, addressstreetnumber, addressstreetdirection, addressstreetname, addressstreetsuffix);
    }


}
