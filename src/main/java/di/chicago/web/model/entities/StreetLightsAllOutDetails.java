package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "street_lights_all_out_details", schema = "public", catalog = "DI_Chicago_")
public class StreetLightsAllOutDetails extends SuperDetails{

    public StreetLightsAllOutDetails(StreetLightsAllOutDetails other) {
    }

    public StreetLightsAllOutDetails(PostRequestParameter other) {
    }

    public StreetLightsAllOutDetails() {
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreetLightsAllOutDetails that = (StreetLightsAllOutDetails) o;
        return requestdetailsid == that.requestdetailsid;
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid);
    }

}
