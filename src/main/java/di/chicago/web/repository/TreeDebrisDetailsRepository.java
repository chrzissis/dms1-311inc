package di.chicago.web.repository;

import di.chicago.web.model.entities.TreeDebrisDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreeDebrisDetailsRepository extends JpaRepository<TreeDebrisDetails, Integer> {
}
