package di.chicago.web.model.entities;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "request", schema = "public", catalog = "DI_Chicago_")
public class Request {
    private int requestid;
    private String servicerequestnumber;
    private Date createdon;
    private Date updatedon;
    private Date completedon;
    private String streetaddress;
    private String zipcode;
    private String xcoordinate;
    private String ycoordinate;
    private String ward;
    private String policedistrict;
    private String communityarea;
    private Double latitude;
    private Double longitude;
    private String location;
//    private RequestStatuses status;
//    private RequestTypes type;

    ///Not mapped
    public int status_id;
    public int type_id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="statusid")
    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    @Column(name="typeid")
    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    //

    public Request(){
        createdon=new Date();
        updatedon=createdon;
        status_id=3;
    }

    @Id
    @Column(name = "requestid", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRequestid() {
        return requestid;
    }

    public void setRequestid(int requestid) {
        this.requestid = requestid;
    }

    @GeneratedValue
    @Column(name = "servicerequestnumber")
    public String getServicerequestnumber() {
        return servicerequestnumber;
    }

    public void setServicerequestnumber(String servicerequestnumber) {
        this.servicerequestnumber = servicerequestnumber;
    }

    @Basic
    @Column(name = "createdon")
    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    @Basic
    @Column(name = "updatedon")
    public Date getUpdatedon() {
        return updatedon;
    }

    public void setUpdatedon(Date updatedon) {
        this.updatedon = updatedon;
    }

    @Basic
    @Column(name = "completedon")
    public Date getCompletedon() {
        return completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    @Basic
    @Column(name = "streetaddress")
    public String getStreetaddress() {
        return streetaddress;
    }

    public void setStreetaddress(String streetaddress) {
        this.streetaddress = streetaddress;
    }

    @Basic
    @Column(name = "zipcode")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "xcoordinate")
    public String getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(String xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public String getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(String ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    @Basic
    @Column(name = "ward")
    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    @Basic
    @Column(name = "policedistrict")
    public String getPolicedistrict() {
        return policedistrict;
    }

    public void setPolicedistrict(String policedistrict) {
        this.policedistrict = policedistrict;
    }

    @Basic
    @Column(name = "communityarea")
    public String getCommunityarea() {
        return communityarea;
    }

    public void setCommunityarea(String communityarea) {
        this.communityarea = communityarea;
    }

    @Basic
    @Column(name = "latitude")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return requestid == request.requestid &&
                Objects.equals(servicerequestnumber, request.servicerequestnumber) &&
                Objects.equals(createdon, request.createdon) &&
                Objects.equals(updatedon, request.updatedon) &&
                Objects.equals(completedon, request.completedon) &&
                Objects.equals(streetaddress, request.streetaddress) &&
                Objects.equals(zipcode, request.zipcode) &&
                Objects.equals(xcoordinate, request.xcoordinate) &&
                Objects.equals(ycoordinate, request.ycoordinate) &&
                Objects.equals(ward, request.ward) &&
                Objects.equals(policedistrict, request.policedistrict) &&
                Objects.equals(communityarea, request.communityarea) &&
                Objects.equals(latitude, request.latitude) &&
                Objects.equals(longitude, request.longitude) &&
                Objects.equals(location, request.location);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestid, servicerequestnumber, createdon, updatedon, completedon, streetaddress, zipcode, xcoordinate, ycoordinate, ward, policedistrict, communityarea, latitude, longitude, location);
    }


    public boolean hasSearchInput (){

        return ( streetaddress != null && streetaddress.length() > 0) ||
                ( zipcode != null && zipcode.length() > 0) ||
                ( servicerequestnumber != null && servicerequestnumber.length() > 0 );

    }
/*
    @ManyToOne
    @JoinColumn(name = "statusid")
    public RequestStatuses getStatus() {
        return status;
    }

    public void setStatus(RequestStatuses status) {
        this.status = status;
    }


    @ManyToOne
    @JoinColumn(name = "typeid")
    public RequestTypes getType() {
        return type;
    }

    public void setType(RequestTypes type) {
        this.type = type;
    }
    */
}
