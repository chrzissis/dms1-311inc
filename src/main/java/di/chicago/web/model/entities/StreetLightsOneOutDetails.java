package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "street_lights_one_out_details", schema = "public", catalog = "DI_Chicago_")
public class StreetLightsOneOutDetails extends SuperDetails{

    public StreetLightsOneOutDetails(StreetLightsOneOutDetails other) {
    }

    public StreetLightsOneOutDetails(PostRequestParameter other) {
    }

    public StreetLightsOneOutDetails() {
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreetLightsOneOutDetails that = (StreetLightsOneOutDetails) o;
        return requestdetailsid == that.requestdetailsid;
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid);
    }


}
