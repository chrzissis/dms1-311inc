package di.chicago.web.repository;

import di.chicago.web.model.entities.RodentBaitingDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RodentBaitingDetailsRepository extends JpaRepository<RodentBaitingDetails, Integer> {
}
