package di.chicago.web.repository;

import di.chicago.web.model.entities.SuperDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailRepository extends JpaRepository<SuperDetails, Integer> {

}
