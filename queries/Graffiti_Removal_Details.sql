
drop table Graffiti_Removal_Details
create table Graffiti_Removal_Details (
                                        RequestDetailsId serial primary key,RequestId int,
                                        TypeofSurface varchar(1000) null,
                                        GraffitiLocation varchar(1000) null,
                                        SSA varchar(1000) null,
                                        FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Graffiti_Removal_Details (RequestId, TypeofSurface, GraffitiLocation,SSA )
select r.RequestId, TypeofSurface, GraffitiLocation,SSA
from Graffiti_Removal gr
inner join request r on r.servicerequestnumber = gr.servicerequestnumber
