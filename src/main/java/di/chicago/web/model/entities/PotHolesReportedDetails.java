package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "pot_holes_reported_details", schema = "public", catalog = "DI_Chicago_")
public class PotHolesReportedDetails extends SuperDetails{
    private String currentactivity;
    private String mostrecentaction;
    private String numberofpotholesfilledonblock;
    private String ssa;

    public PotHolesReportedDetails() {
    }

    public PotHolesReportedDetails(PostRequestParameter other) {
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.numberofpotholesfilledonblock = other.numberofpotholesfilledonblock;
        this.ssa = other.ssa;
    }

    public PotHolesReportedDetails(PotHolesReportedDetails other) {
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.numberofpotholesfilledonblock = other.numberofpotholesfilledonblock;
        this.ssa = other.ssa;
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "currentactivity")
    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    @Basic
    @Column(name = "mostrecentaction")
    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    @Basic
    @Column(name = "numberofpotholesfilledonblock")
    public String getNumberofpotholesfilledonblock() {
        return numberofpotholesfilledonblock;
    }

    public void setNumberofpotholesfilledonblock(String numberofpotholesfilledonblock) {
        this.numberofpotholesfilledonblock = numberofpotholesfilledonblock;
    }

    @Basic
    @Column(name = "ssa")
    public String getSsa() {
        return ssa;
    }

    public void setSsa(String ssa) {
        this.ssa = ssa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PotHolesReportedDetails that = (PotHolesReportedDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(currentactivity, that.currentactivity) &&
                Objects.equals(mostrecentaction, that.mostrecentaction) &&
                Objects.equals(numberofpotholesfilledonblock, that.numberofpotholesfilledonblock) &&
                Objects.equals(ssa, that.ssa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, currentactivity, mostrecentaction, numberofpotholesfilledonblock, ssa);
    }

}
