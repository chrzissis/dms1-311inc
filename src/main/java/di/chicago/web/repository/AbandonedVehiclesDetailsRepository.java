package di.chicago.web.repository;

import di.chicago.web.model.entities.AbandonedVehiclesDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbandonedVehiclesDetailsRepository extends JpaRepository<AbandonedVehiclesDetails, Integer> {
}
