package di.chicago.web.service;

import di.chicago.web.model.entities.*;
import di.chicago.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;




@Service
public class DetailServiceImpl implements DetailService {
    @Autowired
    private DetailRepository detailRepository;

    @Autowired
    private RequestRepository requestRepository;


    @Autowired
    AbandonedVehiclesDetailsRepository abandonedVehiclesDetailsRepository;
    @Autowired
    AlleyLightsOutDetailsRepository alleyLightsOutDetailsRepository;
    @Autowired
    GarbageCatrsDetailsRepository garbageCatrsDetailsRepository;
    @Autowired
    GraffitiRemovalDetailsRepository graffitiRemovalDetailsRepository;
    @Autowired
    PotHolesReportedDetailsRepository potHolesReportedDetailsRepository;
    @Autowired
    RodentBaitingDetailsRepository rodentBaitingDetailsRepository;
    @Autowired
    SanitationCodeComplaintsDetailsRepository sanitationCodeComplaintsDetailsRepository;
    @Autowired
    StreetLightsAllOutDetailsRepository streetLightsAllOutDetailsRepository;
    @Autowired
    StreetLightsOneOutDetailsRepository streetLightsOneOutDetailsRepository;
    @Autowired
    TreeDebrisDetailsRepository treeDebrisDetailsRepository;
    @Autowired
    TreeTrimsDetailsRepository treeTrimsDetailsRepository;
    @Autowired
    VacantAndAbandonedBuildingReportedDetailsRepository vacantAndAbandonedBuildingReportedDetailsRepository;


    @Override
    public void save(SuperDetails details, Request request) {

        details.setRequest(request.getRequestid());
        //details.getSaved(detailRepository);
        Integer typeId = request.getType_id();
        switch(typeId) {
            case 1 :
                abandonedVehiclesDetailsRepository.save((AbandonedVehiclesDetails)details);
                break;
            case 2 :
                 alleyLightsOutDetailsRepository.save((AlleyLightsOutDetails)details);
                break;
            case 3 :
                 garbageCatrsDetailsRepository.save((GarbageCartsDetails)details);
                break;
            case 4 :
                 graffitiRemovalDetailsRepository.save((GraffitiRemovalDetails)details);
                break;
            case 5 :
                 potHolesReportedDetailsRepository.save((PotHolesReportedDetails)details);
                break;
            case 6 :
                 rodentBaitingDetailsRepository.save((RodentBaitingDetails)details);
                break;
            case 7 :
                 sanitationCodeComplaintsDetailsRepository.save((SanitationCodeComplaintsDetails)details);
                break;
            case 8 :
                 streetLightsOneOutDetailsRepository.save((StreetLightsOneOutDetails)details);
                break;
            case 9 :
                 streetLightsAllOutDetailsRepository.save((StreetLightsAllOutDetails)details);
                break;
            case 10 :
                 treeDebrisDetailsRepository.save((TreeDebrisDetails)details);
                break;
            case 11 :
                 treeTrimsDetailsRepository.save((TreeTrimsDetails)details);
                break;
            case 12 :
                 vacantAndAbandonedBuildingReportedDetailsRepository.save((VacantAndAbandonedBuildingReportedDetails)details);
                break;
            default :
                 //streetLightsOneOutDetailsRepository.save((StreetLightsOneOutDetails)details);
                break;
        }
        //detailRepository.save(details);
    }




}