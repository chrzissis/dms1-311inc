package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.*;

@MappedSuperclass
public class SuperDetails {
    private Request request;
    protected int requestdetailsid;
    public int requestid;


    @Column(name = "requestid")
    public int getRequest() {
        return requestid;
    }

    public void setRequest(int requestid) {
        this.requestid = requestid;
    }




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "requestdetailsid", updatable = false, nullable = false)
    public int getRequestdetailsid() {
        return  requestdetailsid;
    }

    public void setRequestdetailsid(int requestdetailsid) {
        this.requestdetailsid = requestdetailsid;
    }
/*
    @ManyToOne
    @JoinColumn(name = "requestid")
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
*/
    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }
}
