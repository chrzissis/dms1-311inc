-- Database: "DI_Chicago_"

-- DROP DATABASE "DI_Chicago_";

--CREATE DATABASE "DI_Chicago_"
--WITH OWNER = postgres
--ENCODING = "UTF8"
--TABLESPACE = pg_default
--LC_COLLATE = "en_US.UTF-8"
--LC_CTYPE = "en_US.UTF-8"
--CONNECTION LIMIT = -1;
/* * /
drop table Abandoned_Vehicles
drop table Alley_Lights_Out
drop table Garbage_Carts
drop table Graffiti_Removal
drop table Pot_Holes_Reported
drop table Rodent_Baiting
drop table Sanitation_Code_Complaints
drop table Street_Lights_All_Out
drop table Street_Lights_One_Out
drop table Tree_Debris
drop table Tree_Trims
drop table Vacant_And_Abandoned_Building_Reported
*/
--------------------------------------------------- Abandoned_Vehicles
--drop table Abandoned_Vehicles

create table Abandoned_Vehicles (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  LicensePlate varchar(4000) null,
  VehicleMakeModel varchar(4000) null,
  VehicleColor varchar(4000) null,
  CurrentActivity varchar(4000) null,
  MostRecentAction varchar(4000) null,
  DaysAbandoned varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  SSA varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);

---------------------------------------------------

--drop table Alley_Lights_Out

create table Alley_Lights_Out (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);



---------------------------------------------------

--drop table Garbage_Carts

create table Garbage_Carts (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  NumberofBlackCartsDelivered varchar(4000) null,
  CurrentActivity varchar(4000) null,
  MostRecentAction varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  SSA varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);



---------------------------------------------------

--drop table Graffiti_Removal

create table Graffiti_Removal (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  TypeofSurface varchar(4000) null,
  GraffitiLocation varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  SSA varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);



---------------------------------------------------

--drop table Pot_Holes_Reported

create table Pot_Holes_Reported (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  CurrentActivity varchar(4000) null,
  MostRecentAction varchar(4000) null,
  NumberofPotholesFilledOnBlock varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  SSA varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);



---------------------------------------------------

--drop table Rodent_Baiting

create table Rodent_Baiting (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  NumberofPremisesBaited varchar(4000) null,
  NumberofPremiseswithGarbage varchar(4000) null,
  NumberofPremiseswithRats varchar(4000) null,
  CurrentActivity varchar(4000) null,
  MostRecentAction varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);


---------------------------------------------------

--drop table Sanitation_Code_Complaints

create table Sanitation_Code_Complaints (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  NatureofCodeViolation varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);


---------------------------------------------------

--drop table Street_Lights_All_Out

create table Street_Lights_All_Out (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);

---------------------------------------------------

--drop table Street_Lights_One_Out

create table Street_Lights_One_Out (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);


---------------------------------------------------

--drop table Tree_Debris

create table Tree_Debris (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  DebrisLocation varchar(4000) null,
  CurrentActivity varchar(4000) null,
  MostRecentAction varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);


---------------------------------------------------

--drop table Tree_Trims

create table Tree_Trims (
  CreatedOn varchar(4000) null,
  Status varchar(4000) null,
  CompletedOn varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  TypeofServiceRequest varchar(4000) null,
  LocationofTrees varchar(4000) null,
  StreetAddress varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);


---------------------------------------------------

--drop table Vacant_And_Abandoned_Building_Reported

create table Vacant_And_Abandoned_Building_Reported (
  ServiceRequestType varchar(4000) null,
  ServiceRequestNumber varchar(4000) null,
  CreatedOn varchar(4000) null,
  BuildingLocation_LOT_BGD varchar(4000) null,
  DangerousorHazardous varchar(4000) null,
  OpenOrBoarded varchar(4000) null,
  IfOpenEntryPoint varchar(4000) null,
  VacantOrOccupied varchar(4000) null,
  VacantDueToFire varchar(4000) null,
  PeopleUsingProperty varchar(4000) null,
  AddressStreetNumber varchar(4000) null,
  AddressStreetDirection varchar(4000) null,
  AddressStreetName varchar(4000) null,
  AddressStreetSuffix varchar(4000) null,
  ZIPCode varchar(4000) null,
  XCoordinate varchar(4000) null,
  YCoordinate varchar(4000) null,
  Ward varchar(4000) null,
  PoliceDistrict varchar(4000) null,
  CommunityArea varchar(4000) null,
  Latitude varchar(4000) null,
  Longitude varchar(4000) null,
  Location varchar(4000) null
);
