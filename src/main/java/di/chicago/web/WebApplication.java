package di.chicago.web;

import di.chicago.web.repository.DetailRepository;
import di.chicago.web.repository.RequestRepository;
import di.chicago.web.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages={"di.chicago.web","di.chicago.web.model", "di.chicago.web.repository", "di.chicago.web.service", "di.chicago.web.validator", "di.chicago.web.web","org.springframework.security.authentication"})
@EnableJpaRepositories(basePackageClasses = {UserRepository.class, RequestRepository.class, DetailRepository.class})
public class WebApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebApplication.class, args);
    }
}