package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "graffiti_removal_details", schema = "public", catalog = "DI_Chicago_")
public class GraffitiRemovalDetails extends SuperDetails{
    private String typeofsurface;
    private String graffitilocation;
    private String ssa;

    public GraffitiRemovalDetails() {
    }

    public GraffitiRemovalDetails(PostRequestParameter other) {
        this.typeofsurface = other.typeofsurface;
        this.graffitilocation = other.graffitilocation;
        this.ssa = other.ssa;
    }

    public GraffitiRemovalDetails(GraffitiRemovalDetails other) {
        this.typeofsurface = other.typeofsurface;
        this.graffitilocation = other.graffitilocation;
        this.ssa = other.ssa;
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "typeofsurface")
    public String getTypeofsurface() {
        return typeofsurface;
    }

    public void setTypeofsurface(String typeofsurface) {
        this.typeofsurface = typeofsurface;
    }

    @Basic
    @Column(name = "graffitilocation")
    public String getGraffitilocation() {
        return graffitilocation;
    }

    public void setGraffitilocation(String graffitilocation) {
        this.graffitilocation = graffitilocation;
    }

    @Basic
    @Column(name = "ssa")
    public String getSsa() {
        return ssa;
    }

    public void setSsa(String ssa) {
        this.ssa = ssa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraffitiRemovalDetails that = (GraffitiRemovalDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(typeofsurface, that.typeofsurface) &&
                Objects.equals(graffitilocation, that.graffitilocation) &&
                Objects.equals(ssa, that.ssa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, typeofsurface, graffitilocation, ssa);
    }

}
