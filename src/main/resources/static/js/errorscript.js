$(document).ready(function(){

    function initErrorHandler (){

        var $wrapper = $('.error-page');

        if($wrapper.length < 1) return;

        $('.js-back-link').on('click', function(e){
            e.preventDefault();
            window.history.back();
        });

        var d = new Date();
        $wrapper.append("<br/>");
        $wrapper.append(d);
    }

    initErrorHandler();


});