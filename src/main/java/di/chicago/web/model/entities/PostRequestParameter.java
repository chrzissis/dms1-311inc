package di.chicago.web.model.entities;

import javax.persistence.Basic;


public class PostRequestParameter {
    public int getRequestdetailsid() {
        return requestdetailsid;
    }

    public void setRequestdetailsid(int requestdetailsid) {
        this.requestdetailsid = requestdetailsid;
    }

    public int getRequestid() {
        return requestid;
    }

    public void setRequestid(int requestid) {
        this.requestid = requestid;
    }

    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }

    public String getVehiclemakemodel() {
        return vehiclemakemodel;
    }

    public void setVehiclemakemodel(String vehiclemakemodel) {
        this.vehiclemakemodel = vehiclemakemodel;
    }

    public String getVehiclecolor() {
        return vehiclecolor;
    }

    public void setVehiclecolor(String vehiclecolor) {
        this.vehiclecolor = vehiclecolor;
    }

    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    public String getDaysabandoned() {
        return daysabandoned;
    }

    public void setDaysabandoned(String daysabandoned) {
        this.daysabandoned = daysabandoned;
    }

    public String getSsa() {
        return ssa;
    }

    public void setSsa(String ssa) {
        this.ssa = ssa;
    }

    public String getNumberofblackcartsdelivered() {
        return numberofblackcartsdelivered;
    }

    public void setNumberofblackcartsdelivered(String numberofblackcartsdelivered) {
        this.numberofblackcartsdelivered = numberofblackcartsdelivered;
    }

    public String getTypeofsurface() {
        return typeofsurface;
    }

    public void setTypeofsurface(String typeofsurface) {
        this.typeofsurface = typeofsurface;
    }

    public String getGraffitilocation() {
        return graffitilocation;
    }

    public void setGraffitilocation(String graffitilocation) {
        this.graffitilocation = graffitilocation;
    }

    public String getNumberofpotholesfilledonblock() {
        return numberofpotholesfilledonblock;
    }

    public void setNumberofpotholesfilledonblock(String numberofpotholesfilledonblock) {
        this.numberofpotholesfilledonblock = numberofpotholesfilledonblock;
    }

    public Integer getNumberofpremisesbaited() {
        return numberofpremisesbaited;
    }

    public void setNumberofpremisesbaited(Integer numberofpremisesbaited) {
        this.numberofpremisesbaited = numberofpremisesbaited;
    }

    public Integer getNumberofpremiseswithgarbage() {
        return numberofpremiseswithgarbage;
    }

    public void setNumberofpremiseswithgarbage(Integer numberofpremiseswithgarbage) {
        this.numberofpremiseswithgarbage = numberofpremiseswithgarbage;
    }

    public Integer getNumberofpremiseswithrats() {
        return numberofpremiseswithrats;
    }

    public void setNumberofpremiseswithrats(Integer numberofpremiseswithrats) {
        this.numberofpremiseswithrats = numberofpremiseswithrats;
    }

    public String getNatureofcodeviolation() {
        return natureofcodeviolation;
    }

    public void setNatureofcodeviolation(String natureofcodeviolation) {
        this.natureofcodeviolation = natureofcodeviolation;
    }

    public String getDebrislocation() {
        return debrislocation;
    }

    public void setDebrislocation(String debrislocation) {
        this.debrislocation = debrislocation;
    }

    public String getLocationoftrees() {
        return locationoftrees;
    }

    public void setLocationoftrees(String locationoftrees) {
        this.locationoftrees = locationoftrees;
    }

    public String getServicerequesttype() {
        return servicerequesttype;
    }

    public void setServicerequesttype(String servicerequesttype) {
        this.servicerequesttype = servicerequesttype;
    }

    public String getBuildinglocationLotBgd() {
        return buildinglocationLotBgd;
    }

    public void setBuildinglocationLotBgd(String buildinglocationLotBgd) {
        this.buildinglocationLotBgd = buildinglocationLotBgd;
    }

    public String getDangerousorhazardous() {
        return dangerousorhazardous;
    }

    public void setDangerousorhazardous(String dangerousorhazardous) {
        this.dangerousorhazardous = dangerousorhazardous;
    }

    public String getOpenorboarded() {
        return openorboarded;
    }

    public void setOpenorboarded(String openorboarded) {
        this.openorboarded = openorboarded;
    }

    public String getIfopenentrypoint() {
        return ifopenentrypoint;
    }

    public void setIfopenentrypoint(String ifopenentrypoint) {
        this.ifopenentrypoint = ifopenentrypoint;
    }

    public String getVacantoroccupied() {
        return vacantoroccupied;
    }

    public void setVacantoroccupied(String vacantoroccupied) {
        this.vacantoroccupied = vacantoroccupied;
    }

    public String getVacantduetofire() {
        return vacantduetofire;
    }

    public void setVacantduetofire(String vacantduetofire) {
        this.vacantduetofire = vacantduetofire;
    }

    public String getPeopleusingproperty() {
        return peopleusingproperty;
    }

    public void setPeopleusingproperty(String peopleusingproperty) {
        this.peopleusingproperty = peopleusingproperty;
    }

    public String getAddressstreetnumber() {
        return addressstreetnumber;
    }

    public void setAddressstreetnumber(String addressstreetnumber) {
        this.addressstreetnumber = addressstreetnumber;
    }

    public String getAddressstreetdirection() {
        return addressstreetdirection;
    }

    public void setAddressstreetdirection(String addressstreetdirection) {
        this.addressstreetdirection = addressstreetdirection;
    }

    public String getAddressstreetname() {
        return addressstreetname;
    }

    public void setAddressstreetname(String addressstreetname) {
        this.addressstreetname = addressstreetname;
    }

    public String getAddressstreetsuffix() {
        return addressstreetsuffix;
    }

    public void setAddressstreetsuffix(String addressstreetsuffix) {
        this.addressstreetsuffix = addressstreetsuffix;
    }

    @Basic
    public int requestdetailsid;
    @Basic
    public int requestid;

    @Basic
    public String licenseplate;
    @Basic
    public String vehiclemakemodel;
    @Basic
    public String vehiclecolor;
    @Basic
    public String currentactivity;
    @Basic
    public String mostrecentaction;
    @Basic
    public String daysabandoned;
    @Basic
    public String ssa;
    @Basic
    public String numberofblackcartsdelivered;

    @Basic
    public String typeofsurface;
    @Basic
    public String graffitilocation;
    @Basic
    public String numberofpotholesfilledonblock;
    @Basic
    public Integer numberofpremisesbaited;
    @Basic
    public Integer numberofpremiseswithgarbage;
    @Basic
    public Integer numberofpremiseswithrats;
    @Basic
    public String natureofcodeviolation;
    @Basic
    public String debrislocation;
    @Basic
    public String locationoftrees;
    @Basic
    public String servicerequesttype;
    @Basic
    public String buildinglocationLotBgd;
    @Basic
    public String dangerousorhazardous;
    @Basic
    public String openorboarded;
    @Basic
    public String ifopenentrypoint;
    @Basic
    public String vacantoroccupied;
    @Basic
    public String vacantduetofire;
    @Basic
    public String peopleusingproperty;
    @Basic
    public String addressstreetnumber;
    @Basic
    public String addressstreetdirection;
    @Basic
    public String addressstreetname;
    @Basic
    public String addressstreetsuffix;
}
