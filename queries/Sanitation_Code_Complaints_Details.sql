drop table Sanitation_Code_Complaints_Details;
create table Sanitation_Code_Complaints_Details (
  RequestDetailsId serial primary key,RequestId int,
  NatureOfCodeViolation varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Sanitation_Code_Complaints_Details (RequestId, NatureofCodeViolation)
select r.RequestId,NatureofCodeViolation
from sanitation_code_complaints scc
inner join request r on r.servicerequestnumber = scc.servicerequestnumber