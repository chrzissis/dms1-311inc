drop table Tree_Debris_Details;
create table Tree_Debris_Details (
 RequestDetailsId serial primary key,RequestId int,
 DebrisLocation varchar(1000) null,
 CurrentActivity varchar(1000) null,
 MostRecentAction varchar(1000) null,
 FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Tree_Debris_Details(RequestId,DebrisLocation,CurrentActivity,MostRecentAction)
select r.RequestId,DebrisLocation,CurrentActivity,MostRecentAction
from tree_debris td
inner join request r on r.servicerequestnumber = td.servicerequestnumber
