with inputRequests as (
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Abandoned_Vehicles
union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Alley_Lights_Out

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Garbage_Carts

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Graffiti_Removal

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Pot_Holes_Reported

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Rodent_Baiting


union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Sanitation_Code_Complaints


union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Street_Lights_All_Out

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Street_Lights_One_Out


union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Tree_Debris

union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,case
when Status = 'Open' then 'open'
when Status = 'Open - Dup' then 'open-dup'
when Status = 'Completed' then 'closed'
when Status = 'Completed - Dup' then 'closed-dup'
end Status , CompletedOn, case
when TypeofServiceRequest = 'Pothole in Street' or TypeofServiceRequest = 'Pot Hole in Street' then 'pothole'
when TypeofServiceRequest = 'Tree Debris' then 'tree-debris'
when TypeofServiceRequest = 'Graffiti Removal' then 'graffiti'
when TypeofServiceRequest = 'Street Light Out' or TypeofServiceRequest = 'Street Light - 1/Out' then 'street-one-out'
when TypeofServiceRequest = 'Street Lights - All/Out' then 'street-all-out'
when TypeofServiceRequest = 'Garbage Cart Black Maintenance/Replacement' then 'garbage-cart'
when TypeofServiceRequest = 'Alley Light Out' then 'alley-one-out'
when TypeofServiceRequest = 'Abandoned Vehicle Complaint' then 'abandoned-vehicle'
when TypeofServiceRequest = 'Vacant/Abandoned Building' then 'abandoned-building'
when TypeofServiceRequest = 'Tree Trim' then 'tree-trim'
when TypeofServiceRequest = 'Rodent Baiting/Rat Complaint' then 'rodent-rat'
when TypeofServiceRequest = 'Sanitation Code Violation' then 'sanitation'

end TypeId, StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Tree_Trims


union
select ServiceRequestNumber,CreatedOn,null UpdatedOn,'open' Status , null CompletedOn, 'abandoned-building' TypeId,'' StreetAddress, ZIPCode, XCoordinate, YCoordinate, Ward, PoliceDistrict, CommunityArea, Latitude, Longitude, Location
from Vacant_And_Abandoned_Building_Reported

)
insert into Request (ServiceRequestNumber,
CreatedOn,
UpdatedOn,
StatusId,
CompletedOn,
TypeId,
StreetAddress,
ZIPCode,
XCoordinate,
YCoordinate,
Ward,
PoliceDistrict,
CommunityArea,
Latitude,
Longitude,
Location)
select ServiceRequestNumber,
       cast ( CreatedOn as date ) CreatedOn,
       cast ( UpdatedOn as date ) UpdatedOn,
       coalesce( (select StatusId from Request_Statuses rs where rs.Code = ir.Status ),1) StatusId,
       cast ( CompletedOn as date ) CompletedOn,
       coalesce( (select TypeId from Request_Types rt where rt.Code = ir.TypeId ),1) TypeId,
       StreetAddress,
       ZIPCode,
       XCoordinate,
       YCoordinate,
       Ward,
       PoliceDistrict,
       CommunityArea,
       cast (Latitude as double precision ) Latitude,
       cast (Longitude as double precision ) Longitude,
       Location
from inputRequests ir
/*4266592*/

