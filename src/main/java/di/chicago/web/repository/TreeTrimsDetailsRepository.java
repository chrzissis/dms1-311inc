package di.chicago.web.repository;

import di.chicago.web.model.entities.TreeTrimsDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreeTrimsDetailsRepository extends JpaRepository<TreeTrimsDetails, Integer> {
}
