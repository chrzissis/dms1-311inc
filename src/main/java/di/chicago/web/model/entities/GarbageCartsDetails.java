package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "garbage_carts_details", schema = "public", catalog = "DI_Chicago_")
public class GarbageCartsDetails extends SuperDetails{
    private String numberofblackcartsdelivered;
    private String currentactivity;
    private String mostrecentaction;
    private String ssa;

    public GarbageCartsDetails() {
    }

    public GarbageCartsDetails(PostRequestParameter other) {
        this.numberofblackcartsdelivered = other.numberofblackcartsdelivered;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.ssa = other.ssa;
    }

    public GarbageCartsDetails(GarbageCartsDetails other) {
        this.numberofblackcartsdelivered = other.numberofblackcartsdelivered;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.ssa = other.ssa;
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "numberofblackcartsdelivered")
    public String getNumberofblackcartsdelivered() {
        return numberofblackcartsdelivered;
    }

    public void setNumberofblackcartsdelivered(String numberofblackcartsdelivered) {
        this.numberofblackcartsdelivered = numberofblackcartsdelivered;
    }

    @Basic
    @Column(name = "currentactivity")
    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    @Basic
    @Column(name = "mostrecentaction")
    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    @Basic
    @Column(name = "ssa")
    public String getSsa() {
        return ssa;
    }

    public void setSsa(String ssa) {
        this.ssa = ssa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GarbageCartsDetails that = (GarbageCartsDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(numberofblackcartsdelivered, that.numberofblackcartsdelivered) &&
                Objects.equals(currentactivity, that.currentactivity) &&
                Objects.equals(mostrecentaction, that.mostrecentaction) &&
                Objects.equals(ssa, that.ssa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, numberofblackcartsdelivered, currentactivity, mostrecentaction, ssa);
    }

}
