package di.chicago.web.repository;

import di.chicago.web.model.entities.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {
    Request removeByRequestid(Long requestid);


}
