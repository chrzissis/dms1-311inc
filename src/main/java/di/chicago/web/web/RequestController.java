package di.chicago.web.web;

import di.chicago.web.model.entities.*;
import di.chicago.web.service.DetailService;
import di.chicago.web.service.RequestService;
import di.chicago.web.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.UUID;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Controller
@RequestMapping(value = "/")
public class RequestController {

    @Autowired
    RequestValidator requestValidator;

    @Autowired
    RequestService requestService;


    @Autowired
    DetailService detailService;

    private Logger logger = LoggerFactory.getLogger(RequestController.class);


    @RequestMapping(value = "newRequest", method = RequestMethod.GET)
    public String newRequestGET(Model model, @ModelAttribute(name = "requestId") String requestId){
        Request request;
        Integer reqId = requestId.length() > 0 ? Integer.parseInt(requestId) : 0;
        if(reqId > 0){
            request = requestService.findByRequestID(reqId);
        }else{
            request = new Request();
        }
        model.addAttribute("request",request);

        return "/newRequest.html";
    }

    @RequestMapping(value = "newRequest", method = RequestMethod.POST)
    public String newRequestPOST(@ModelAttribute(name = "request") Request requestObject, BindingResult bindingResult, RedirectAttributes redirectmodel){

        //System.out.println(request.getStatus_id());
        requestValidator.validate(requestObject,bindingResult);
        if(bindingResult.hasErrors()) {
            return "/newRequest.html";
        }
        if(requestObject.getServicerequestnumber() == null || requestObject.getServicerequestnumber().length() == 0) {
            requestObject.setServicerequestnumber(UUID.randomUUID().toString().substring(0,20));
        }


        requestService.save(requestObject);
        redirectmodel.addFlashAttribute("request", requestObject);

        int request=requestObject.getRequestid();

        /*
        Integer typeId = requestObject.getType_id();
        switch(typeId) {
            case 1 :
                AbandonedVehiclesDetails abandonedVehiclesDetails = new AbandonedVehiclesDetails();
                abandonedVehiclesDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", abandonedVehiclesDetails);
                break;
            case 2 :
                AlleyLightsOutDetails alleyLightsOutDetails = new AlleyLightsOutDetails();
                alleyLightsOutDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", alleyLightsOutDetails);
                break;
            case 3 :
                GarbageCartsDetails garbageCartsDetails = new GarbageCartsDetails();
                garbageCartsDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", garbageCartsDetails);
                break;
            case 4 :
                GraffitiRemovalDetails graffitiRemovalDetails = new GraffitiRemovalDetails();
                graffitiRemovalDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", graffitiRemovalDetails);
                break;
            case 5 :
                PotHolesReportedDetails potHolesReportedDetails = new PotHolesReportedDetails();
                potHolesReportedDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", potHolesReportedDetails);
                break;
            case 6 :
                RodentBaitingDetails rodentBaitingDetails = new RodentBaitingDetails();
                rodentBaitingDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", rodentBaitingDetails);
                break;
            case 7 :
                SanitationCodeComplaintsDetails sanitationCodeComplaintsDetails = new SanitationCodeComplaintsDetails();
                sanitationCodeComplaintsDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", sanitationCodeComplaintsDetails);
                break;
            case 8 :
                StreetLightsOneOutDetails streetLightsOneOutDetails = new StreetLightsOneOutDetails();
                streetLightsOneOutDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", streetLightsOneOutDetails);
                break;
            case 9 :
                StreetLightsAllOutDetails streetLightsAllOutDetails = new StreetLightsAllOutDetails();
                streetLightsAllOutDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", streetLightsAllOutDetails);
                break;
            case 10 :
                TreeDebrisDetails treeDebrisDetails = new TreeDebrisDetails();
                treeDebrisDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", treeDebrisDetails);
                break;
            case 11 :
                TreeTrimsDetails treeTrimsDetails = new TreeTrimsDetails();
                treeTrimsDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", treeTrimsDetails);
                break;
            case 12 :
                VacantAndAbandonedBuildingReportedDetails vacantAndAbandonedBuildingReportedDetails = new VacantAndAbandonedBuildingReportedDetails();
                vacantAndAbandonedBuildingReportedDetails.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", vacantAndAbandonedBuildingReportedDetails);
                break;
            default :
                StreetLightsOneOutDetails default_ = new StreetLightsOneOutDetails();
                default_.setRequest(request);
                redirectmodel.addFlashAttribute("requestDetails", default_);

                break;
        }
        */
        PostRequestParameter postRequestParameter= new PostRequestParameter();
        postRequestParameter.requestid=requestObject.getRequestid();
        redirectmodel.addFlashAttribute("requestDetails", postRequestParameter);

        return "redirect:/newRequestDetails";
    }

    @RequestMapping(value = "newRequestDetails", method = RequestMethod.GET)
    public String newRequestDetailsGET(Model model){
        //model.addAttribute("request",request);
        return "/newRequestDetails.html";
    }

    @RequestMapping(value = "newRequestDetails", method = RequestMethod.POST)
    public String newRequestDetailsPOST(@ModelAttribute(name = "requestDetails") PostRequestParameter requestDetails, BindingResult bindingResult, RedirectAttributes redirectmodel){
        Request request = requestService.findByRequestID(requestDetails.requestid);
        if(request==null){
            redirectmodel.addFlashAttribute("newRequestSuccess", "Your last request was not submitted");
            return "redirect:/welcome";

        }


        //Request request= requestService.findByRequestID(superDetails.getRequest());
        Integer typeId = request.getType_id();
        switch(typeId) {
            case 1 :
                AbandonedVehiclesDetails detailsObject = new AbandonedVehiclesDetails(requestDetails);
                detailService.save(detailsObject,request);
                break;
            case 2 :
                AlleyLightsOutDetails detailsObject1 = new AlleyLightsOutDetails( requestDetails);
                detailService.save(detailsObject1,request);
                break;
            case 3 :
                GarbageCartsDetails detailsObject2 = new GarbageCartsDetails(requestDetails);
                detailService.save(detailsObject2,request);
                break;
            case 4 :
                GraffitiRemovalDetails detailsObject3 = new GraffitiRemovalDetails(requestDetails);
                detailService.save(detailsObject3,request);
                break;
            case 5 :
                PotHolesReportedDetails detailsObject4 = new PotHolesReportedDetails(requestDetails);
                detailService.save(detailsObject4,request);
                break;
            case 6 :
                RodentBaitingDetails detailsObject5 = new RodentBaitingDetails(requestDetails);
                detailService.save(detailsObject5,request);
                break;
            case 7 :
                SanitationCodeComplaintsDetails detailsObject6 = new SanitationCodeComplaintsDetails(requestDetails);
                detailService.save(detailsObject6,request);
                break;
            case 8 :
                StreetLightsOneOutDetails detailsObject7 = new StreetLightsOneOutDetails(requestDetails);
                detailService.save(detailsObject7,request);
                break;
            case 9 :
                StreetLightsAllOutDetails detailsObject8 = new StreetLightsAllOutDetails(requestDetails);
                detailService.save(detailsObject8,request);
                break;
            case 10 :
                TreeDebrisDetails detailsObject9 = new TreeDebrisDetails(requestDetails);
                detailService.save(detailsObject9,request);
                break;
            case 11 :
                TreeTrimsDetails detailsObject10 = new TreeTrimsDetails(requestDetails);
                detailService.save(detailsObject10,request);
                break;
            case 12 :
                VacantAndAbandonedBuildingReportedDetails detailsObject11 = new VacantAndAbandonedBuildingReportedDetails(requestDetails);
                detailService.save(detailsObject11,request);
                break;
            default :
                StreetLightsOneOutDetails detailsObject12 = new StreetLightsOneOutDetails(requestDetails);
                detailService.save(detailsObject12,request);
                break;
        }



        String serviceRequestNumber= "";
        try{
            serviceRequestNumber = request.getServicerequestnumber();
        }catch (Exception ex){
            System.out.println("no request");
        }
        redirectmodel.addFlashAttribute("newRequestSuccess", "Your last request was submitted with service number : "+ serviceRequestNumber);
        //detailService.save(temp, request);
        return "redirect:/welcome";
    }


    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String search(@ModelAttribute(value = "searchInput") Request searchInput,@ModelAttribute(value = "pageNo") String pageNo, Model model) {
        List<Request> searchResults=null;
        if(searchInput==null || !searchInput.hasSearchInput()) {
            model.addAttribute("searchInput", new Request());

        }
        else{
            Integer page = pageNo.length() > 0 ? Integer.parseInt(pageNo) : 0;
            searchResults = requestService.getQueryPage(page, requestService.searchRequest(searchInput));
            model.addAttribute("search",searchResults);

            model.addAttribute("PageNum", pageNo);

            if(searchResults==null || searchResults.size() == 0) {
                model.addAttribute("ResultText", "There were no results");

            }
            if(searchResults.size() == 20) {
                model.addAttribute("NextPageUrl","/search?streetaddress="+ searchInput.getStreetaddress() +"&zipcode="+searchInput.getZipcode()+"&servicerequestnumber="+searchInput.getServicerequestnumber()+"&pageNo="+(page+1));
            }
            if(page >= 1){
                model.addAttribute("PrevPageUrl", "/search?streetaddress="+ searchInput.getStreetaddress() +"&zipcode="+searchInput.getZipcode()+"&servicerequestnumber="+searchInput.getServicerequestnumber()+"&pageNo="+(page-1));
            }
        }

        return "/search.html";


    }



}
