package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tree_debris_details", schema = "public", catalog = "DI_Chicago_")
public class TreeDebrisDetails extends SuperDetails{
    private String debrislocation;
    private String currentactivity;
    private String mostrecentaction;

    public TreeDebrisDetails(TreeDebrisDetails other) {
        this.debrislocation = other.debrislocation;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
    }

    public TreeDebrisDetails(PostRequestParameter other) {
        this.debrislocation = other.debrislocation;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
    }

    public TreeDebrisDetails() {
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "debrislocation")
    public String getDebrislocation() {
        return debrislocation;
    }

    public void setDebrislocation(String debrislocation) {
        this.debrislocation = debrislocation;
    }

    @Basic
    @Column(name = "currentactivity")
    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    @Basic
    @Column(name = "mostrecentaction")
    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeDebrisDetails that = (TreeDebrisDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(debrislocation, that.debrislocation) &&
                Objects.equals(currentactivity, that.currentactivity) &&
                Objects.equals(mostrecentaction, that.mostrecentaction);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, debrislocation, currentactivity, mostrecentaction);
    }


}
