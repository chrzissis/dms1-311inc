drop table Tree_Trims_Details;
create table Tree_Trims_Details (
  RequestDetailsId serial primary key,RequestId int,
  LocationOfTrees varchar(1000) null,
  FOREIGN KEY (RequestId) REFERENCES Request (RequestId)
);
insert into Tree_Trims_Details(RequestId,LocationOfTrees)
select r.RequestId,LocationOfTrees
from tree_trims tt
inner join request r on r.servicerequestnumber = tt.servicerequestnumber
