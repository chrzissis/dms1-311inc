package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tree_trims_details", schema = "public", catalog = "DI_Chicago_")
public class TreeTrimsDetails extends SuperDetails{
    private String locationoftrees;

    public TreeTrimsDetails() {
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }


    public TreeTrimsDetails(PostRequestParameter other) {
        this.locationoftrees = other.locationoftrees;
    }

    public TreeTrimsDetails(TreeTrimsDetails other) {
        this.locationoftrees = other.locationoftrees;
    }

    @Basic
    @Column(name = "locationoftrees")
    public String getLocationoftrees() {
        return locationoftrees;
    }

    public void setLocationoftrees(String locationoftrees) {
        this.locationoftrees = locationoftrees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeTrimsDetails that = (TreeTrimsDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(locationoftrees, that.locationoftrees);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, locationoftrees);
    }


}
