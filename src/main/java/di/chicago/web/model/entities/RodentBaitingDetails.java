package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "rodent_baiting_details", schema = "public", catalog = "DI_Chicago_")
public class RodentBaitingDetails extends SuperDetails{
    private Integer numberofpremisesbaited;
    private Integer numberofpremiseswithgarbage;
    private Integer numberofpremiseswithrats;
    private String currentactivity;
    private String mostrecentaction;

    public RodentBaitingDetails() {
    }

    public RodentBaitingDetails(PostRequestParameter other) {
        this.numberofpremisesbaited = other.numberofpremisesbaited;
        this.numberofpremiseswithgarbage = other.numberofpremiseswithgarbage;
        this.numberofpremiseswithrats = other.numberofpremiseswithrats;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
    }

    public RodentBaitingDetails(RodentBaitingDetails other) {
        this.numberofpremisesbaited = other.numberofpremisesbaited;
        this.numberofpremiseswithgarbage = other.numberofpremiseswithgarbage;
        this.numberofpremiseswithrats = other.numberofpremiseswithrats;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }

    @Basic
    @Column(name = "numberofpremisesbaited")
    public Integer getNumberofpremisesbaited() {
        return numberofpremisesbaited;
    }

    public void setNumberofpremisesbaited(Integer numberofpremisesbaited) {
        this.numberofpremisesbaited = numberofpremisesbaited;
    }

    @Basic
    @Column(name = "numberofpremiseswithgarbage")
    public Integer getNumberofpremiseswithgarbage() {
        return numberofpremiseswithgarbage;
    }

    public void setNumberofpremiseswithgarbage(Integer numberofpremiseswithgarbage) {
        this.numberofpremiseswithgarbage = numberofpremiseswithgarbage;
    }

    @Basic
    @Column(name = "numberofpremiseswithrats")
    public Integer getNumberofpremiseswithrats() {
        return numberofpremiseswithrats;
    }

    public void setNumberofpremiseswithrats(Integer numberofpremiseswithrats) {
        this.numberofpremiseswithrats = numberofpremiseswithrats;
    }

    @Basic
    @Column(name = "currentactivity")
    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    @Basic
    @Column(name = "mostrecentaction")
    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RodentBaitingDetails that = (RodentBaitingDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(numberofpremisesbaited, that.numberofpremisesbaited) &&
                Objects.equals(numberofpremiseswithgarbage, that.numberofpremiseswithgarbage) &&
                Objects.equals(numberofpremiseswithrats, that.numberofpremiseswithrats) &&
                Objects.equals(currentactivity, that.currentactivity) &&
                Objects.equals(mostrecentaction, that.mostrecentaction);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, numberofpremisesbaited, numberofpremiseswithgarbage, numberofpremiseswithrats, currentactivity, mostrecentaction);
    }

}
