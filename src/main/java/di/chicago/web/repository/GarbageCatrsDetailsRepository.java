package di.chicago.web.repository;

import di.chicago.web.model.entities.GarbageCartsDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GarbageCatrsDetailsRepository extends JpaRepository<GarbageCartsDetails, Integer> {
}
