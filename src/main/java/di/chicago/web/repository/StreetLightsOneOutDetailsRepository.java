package di.chicago.web.repository;

import di.chicago.web.model.entities.StreetLightsOneOutDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StreetLightsOneOutDetailsRepository extends JpaRepository<StreetLightsOneOutDetails, Integer> {
}
