package di.chicago.web.service;

import di.chicago.web.model.User;
import di.chicago.web.model.entities.Request;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

@Service
public interface RequestService {
    void save(Request request);

    void removeById(Long id);

    Request findByRequestID(int id);
    public TypedQuery searchRequest(Request search);
    public List<Request> getQueryPage(int pageNo, TypedQuery query);
}