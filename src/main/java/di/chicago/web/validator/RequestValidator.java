package di.chicago.web.validator;

import di.chicago.web.model.User;
import di.chicago.web.model.entities.Request;
import di.chicago.web.service.RequestService;
import di.chicago.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RequestValidator implements Validator {
    @Autowired
    private RequestService requestService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Request.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Request request = (Request) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type_id", "NotEmpty");

        if(!(request.status_id<=4 && request.status_id>=1))
            errors.rejectValue("status_id", "Invalid.Request.Status");

        if(!(request.type_id<=12 && request.type_id>=1))
            errors.rejectValue("type_id","Invalid.Request.Type");
    }
}