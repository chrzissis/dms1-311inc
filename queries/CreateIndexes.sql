CREATE INDEX Request_CreatedOn_idx ON Request (CreatedOn);
-- drop INDEX Request_CreatedOn_idx
-- this index drop queries 4 times down when it is applied on range queries based on created on date
-- eg example 1 query 360 to 90
-- eg example 2 query 600 to 100

CREATE INDEX Request_TypeId_idx ON Request (TypeId);
CREATE INDEX Request_ZipCode_idx ON Request (ZipCode);
CREATE INDEX Request_Latitude_idx ON Request (latitude);
CREATE INDEX Request_Longitude_idx ON Request (longitude);

CREATE INDEX Request_AV_idx ON Abandoned_Vehicles_Details (RequestId);
CREATE INDEX Request_GC_idx ON Garbage_Carts_Details (RequestId);
CREATE INDEX Request_GR_idx ON Graffiti_Removal_Details (RequestId);
CREATE INDEX Request_PHR_idx ON Pot_Holes_Reported_Details (RequestId);

CREATE INDEX Request_CompletedOn_idx ON Request (CompletedOn);
CREATE UNIQUE INDEX Request_Types_Code_idx ON Request_types (Code);

