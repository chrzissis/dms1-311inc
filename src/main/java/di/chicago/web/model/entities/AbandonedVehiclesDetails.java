package di.chicago.web.model.entities;

import di.chicago.web.repository.DetailRepository;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "abandoned_vehicles_details", schema = "public", catalog = "DI_Chicago_")
public class AbandonedVehiclesDetails extends SuperDetails{
    private String licenseplate;
    private String vehiclemakemodel;
    private String vehiclecolor;
    private String currentactivity;
    private String mostrecentaction;
    private String daysabandoned;
    private String ssa;


    public AbandonedVehiclesDetails(PostRequestParameter other) {
        this.licenseplate = other.licenseplate;
        this.vehiclemakemodel = other.vehiclemakemodel;
        this.vehiclecolor = other.vehiclecolor;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.daysabandoned = other.daysabandoned;
        this.ssa = other.ssa;
    }


    public AbandonedVehiclesDetails(AbandonedVehiclesDetails other) {
        this.licenseplate = other.licenseplate;
        this.vehiclemakemodel = other.vehiclemakemodel;
        this.vehiclecolor = other.vehiclecolor;
        this.currentactivity = other.currentactivity;
        this.mostrecentaction = other.mostrecentaction;
        this.daysabandoned = other.daysabandoned;
        this.ssa = other.ssa;
    }

    public AbandonedVehiclesDetails() {
    }

    public void getSaved(DetailRepository detailRepository){
        detailRepository.save(this);
    }


    @Basic
    @Column(name = "licenseplate")
    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }

    @Basic
    @Column(name = "vehiclemakemodel")
    public String getVehiclemakemodel() {
        return vehiclemakemodel;
    }

    public void setVehiclemakemodel(String vehiclemakemodel) {
        this.vehiclemakemodel = vehiclemakemodel;
    }

    @Basic
    @Column(name = "vehiclecolor")
    public String getVehiclecolor() {
        return vehiclecolor;
    }

    public void setVehiclecolor(String vehiclecolor) {
        this.vehiclecolor = vehiclecolor;
    }

    @Basic
    @Column(name = "currentactivity")
    public String getCurrentactivity() {
        return currentactivity;
    }

    public void setCurrentactivity(String currentactivity) {
        this.currentactivity = currentactivity;
    }

    @Basic
    @Column(name = "mostrecentaction")
    public String getMostrecentaction() {
        return mostrecentaction;
    }

    public void setMostrecentaction(String mostrecentaction) {
        this.mostrecentaction = mostrecentaction;
    }

    @Basic
    @Column(name = "daysabandoned")
    public String getDaysabandoned() {
        return daysabandoned;
    }

    public void setDaysabandoned(String daysabandoned) {
        this.daysabandoned = daysabandoned;
    }

    @Basic
    @Column(name = "ssa")
    public String getSsa() {
        return ssa;
    }

    public void setSsa(String ssa) {
        this.ssa = ssa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbandonedVehiclesDetails that = (AbandonedVehiclesDetails) o;
        return requestdetailsid == that.requestdetailsid &&
                Objects.equals(licenseplate, that.licenseplate) &&
                Objects.equals(vehiclemakemodel, that.vehiclemakemodel) &&
                Objects.equals(vehiclecolor, that.vehiclecolor) &&
                Objects.equals(currentactivity, that.currentactivity) &&
                Objects.equals(mostrecentaction, that.mostrecentaction) &&
                Objects.equals(daysabandoned, that.daysabandoned) &&
                Objects.equals(ssa, that.ssa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestdetailsid, licenseplate, vehiclemakemodel, vehiclecolor, currentactivity, mostrecentaction, daysabandoned, ssa);
    }


}
