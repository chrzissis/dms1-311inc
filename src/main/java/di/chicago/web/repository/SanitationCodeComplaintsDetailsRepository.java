package di.chicago.web.repository;

import di.chicago.web.model.entities.SanitationCodeComplaintsDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanitationCodeComplaintsDetailsRepository extends JpaRepository<SanitationCodeComplaintsDetails, Integer> {
}
