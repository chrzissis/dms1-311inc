/* REMOVING TABLE COLUMN NAMES FROM INPUT DATA*/
select distinct ServiceRequestNumber from Abandoned_Vehicles where servicerequestnumber = 'Service Request Number'
delete from Abandoned_Vehicles where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Alley_Lights_Out where servicerequestnumber = 'Service Request Number'
delete from Alley_Lights_Out where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Garbage_Carts where servicerequestnumber = 'Service Request Number'
delete from Garbage_Carts where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Graffiti_Removal where servicerequestnumber = 'Service Request Number'
delete from Graffiti_Removal where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Pot_Holes_Reported where servicerequestnumber = 'Service Request Number'
delete from Pot_Holes_Reported where servicerequestnumber = 'SERVICE REQUEST NUMBER'

select distinct ServiceRequestNumber from Rodent_Baiting where servicerequestnumber = 'Service Request Number'
delete from Rodent_Baiting where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Sanitation_Code_Complaints where servicerequestnumber = 'Service Request Number'
delete from Sanitation_Code_Complaints where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Street_Lights_All_Out where servicerequestnumber = 'Service Request Number'
delete from Street_Lights_All_Out where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Street_Lights_One_Out where servicerequestnumber = 'Service Request Number'
delete from Street_Lights_One_Out where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Tree_Debris where servicerequestnumber = 'Service Request Number'
delete from Tree_Debris where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Tree_Trims where servicerequestnumber = 'Service Request Number'
delete from Tree_Trims where servicerequestnumber = 'Service Request Number'

select distinct ServiceRequestNumber from Vacant_And_Abandoned_Building_Reported where servicerequestnumber = 'Service Request Number'
delete from Vacant_And_Abandoned_Building_Reported where servicerequestnumber = 'SERVICE REQUEST NUMBER'
