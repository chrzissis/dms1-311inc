package di.chicago.web.service;

import di.chicago.web.model.User;
import di.chicago.web.model.entities.Request;
import di.chicago.web.model.entities.SuperDetails;
import org.springframework.stereotype.Service;

@Service
public interface DetailService {
    void save(SuperDetails details, Request request);
}