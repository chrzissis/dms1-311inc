package di.chicago.web.repository;

import di.chicago.web.model.entities.PotHolesReportedDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PotHolesReportedDetailsRepository extends JpaRepository<PotHolesReportedDetails, Integer> {
}
