package di.chicago.web.repository;

import di.chicago.web.model.entities.GraffitiRemovalDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GraffitiRemovalDetailsRepository extends JpaRepository<GraffitiRemovalDetails, Integer> {
}
