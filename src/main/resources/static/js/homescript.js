$(document).ready(function(){

    function initWelcomeHandler (){

        var $wrapper = $('.home-page');

        if($wrapper.length < 1) return;

        $wrapper.append("<br/>");
        var d = new Date();
        $wrapper.append(d);

        $wrapper.append("<br/>");
        $.ajax({
            contentType: 'application/json',
            method: 'POST',
            data: JSON.stringify({"value": "Panos"}),
            url: '/sampleEndpoint',
            success: function (response){
                console.log(response);
                $wrapper.append(response);
                $wrapper.append("<br/>");
            },
            failure: function (response){
                console.log(response);
                $wrapper.append(response);
                $wrapper.append("<br/>");
            },
            error: function ( response ){
                console.log(response);
                $wrapper.append(response);
                $wrapper.append("<br/>");
            }
        });

    }

    initWelcomeHandler();


});